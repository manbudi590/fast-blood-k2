import React, { Component } from 'react'; 
import Button from '@material-ui/core/Button';
import Header from './header.jsx';
import Jumbo from './jumbo.jsx';
import Example from './card.jsx';


class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Jumbo />
        <Example />
        <Button variant="contained">
        Default
        </Button>
      </div>
    );
  }
}

export default App;
